package cl.ceisufro.easyhttp.core;

import cl.ceisufro.easyhttp.model.Response;

/**
 * Created by Luis Andrés Jara Castillo on 04-09-17.
 *
 * Interface cuya finalidad es la estructurar las alertas
 * que se enviaran desde la tarea asíncrona de la clase {@link Executor}.
 */

public interface TaskCallback {
    /**
     * Método abstracto cuyo fin es marcar el inicio de un proceso o tarea.
     * Es util para modificar la interfaz (por ejemplo, hacer visible una barra de carga)
     * o prepararse para el proceso principal de la tarea previo a su ejecución.
     */
    void onStart();

    /**
     * Método abstracto cuyo fin es marcar el final de un proceso o tarea.
     * Es util para modificar la interfaz (por ejemplo, devolver la interfaz a la normalidad)
     * o realizar cualquier proceso luego de la ejecución de la tarea.
     */
    void onFinish();

    /**
     * Método abstracto cuyo fin es retornar el resultado de la tarea.
     * El resultado es devuelto a través de {@link Response}.
     *
     * @param result {@link Response} resultado de la tarea si se realizo con exito la
     *               solicitud o null si la respuesta no es correcta.
     */
    void onResult(Response result);

    /**
     * Método abstracto cuyo fin es retornar los {@link Throwable} que sean
     * capturados durante la ejecución.
     *
     * @param throwable {@link Throwable} con la información del error capturado durante la ejecución.
     */
    void onError(Throwable throwable);
}
