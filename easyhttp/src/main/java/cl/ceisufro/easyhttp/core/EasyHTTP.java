package cl.ceisufro.easyhttp.core;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import cl.ceisufro.easyhttp.model.RequestType;
import cl.ceisufro.easyhttp.model.Response;

/**
 * Created by Luis Andrés Jara Castillo on 04-09-17.
 * <p>
 * Clase principal para utilizar esta biblioteca.
 * Para crear una nueva instancia de esta clase se debe utilizar ya sea
 * {@link Builder} (Por defecto es una solicitud GET) o {@link PostBuilder} (Para realizar
 * una solicitud de tipo POST y enviar datos).
 */
public class EasyHTTP {
    private int connectTimeout;
    private int readTimeout;
    private String url;
    private boolean forceSSL;
    private RequestType requestType;
    private HashMap<String, String> headerMap;
    private HashMap<String, File> filesMap;
    private HashMap<String, Object> parametersMap;
    private String rawBody;

    /**
     * Constructor de la clase {@link EasyHTTP}.
     * <p>
     * Se crea a partir de una instacia de {@link Builder}.
     *
     * @param builder {@link Builder} para inicializar los valores de {@link EasyHTTP}.
     */
    EasyHTTP(Builder builder) {
        this.connectTimeout = builder.connectTimeout;
        this.readTimeout = builder.readTimeout;
        this.url = builder.url;
        this.forceSSL = builder.forceSSL;
        this.requestType = builder.requestType;
        this.headerMap = builder.headerMap;
        this.filesMap = new HashMap<>();
        this.parametersMap = new HashMap<>();
    }

    /**
     * Constructor de la clase {@link EasyHTTP}.
     * <p>
     * Se crea a partir de una instacia de {@link PostBuilder}.
     *
     * @param builder {@link PostBuilder} para inicializar los valores de {@link EasyHTTP}.
     */
    EasyHTTP(PostBuilder builder) {
        this.connectTimeout = builder.connectTimeout;
        this.readTimeout = builder.readTimeout;
        this.url = builder.url;
        this.forceSSL = builder.forceSSL;
        this.requestType = builder.requestType;
        this.headerMap = builder.headerMap;
        this.filesMap = builder.filesMap;
        this.parametersMap = builder.parametersMap;
        this.rawBody = builder.rawBody;
    }

    /**
     * Retorna el valor establecido para el tiempo de espera de conexión.
     *
     * @return valor entero del tiempo de conexión en segundos.
     */
    public int getConnectTimeout() {
        return connectTimeout;
    }

    /**
     * Retorna el valor establecido para el tiempo de espera de lectura.
     *
     * @return valor entero del tiempo de lectura en segundos.
     */
    public int getReadTimeout() {
        return readTimeout;
    }

    /**
     * Retorna el valor establecido para la URL.
     *
     * @return {@link String} URL a la cual se realizara la solicitud.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Retorna si se ha forzado o no la conexión mediante le protocolo TLS.
     *
     * @return true si se ha forzado o false si no se ha forzado.
     */
    public boolean isSSLForced() {
        return forceSSL;
    }

    /**
     * Retorna el tipo de solicitud que sera llevada a cabo.
     *
     * @return {@link RequestType#GET} o  {@link RequestType#POST}
     */
    public RequestType getRequestType() {
        return requestType;
    }

    /**
     * Retorna el mapa de valores que serán añadidos a la cabecera de la solicitud.
     *
     * @return {@link HashMap} con los valores del header.
     */
    public HashMap<String, String> getHeaderMap() {
        return headerMap;
    }

    /**
     * Retorna el mapa de archivos que serán añadidos al cuerpo de la solicitud.
     *
     * @return {@link HashMap} con los archivos de la solicitud.
     */
    public HashMap<String, File> getFilesMap() {
        return filesMap;
    }

    /**
     * Retorna el mapa de parámetros que serán añadidos al cuerpo de la solicitud.
     *
     * @return {@link HashMap} con los parámetros de la solicitud.
     */
    public HashMap<String, Object> getParametersMap() {
        return parametersMap;
    }

    /**
     * Retorna el cuerpo bruto de la solicitud.
     *
     * @return {@link String} cuerpo bruto de la solicitud.
     */
    public String getRawBody() {
        return rawBody;
    }

    public static Builder newBuilder(String url) {
        return new Builder(url);
    }

    /**
     * Ejecuta un llamado de la solicitud de manera asíncrona.
     * <p>
     * Esto es util debido a que evita congelar un hilo mientras se realiza la ejecución.
     * Además si se hace un llamado desde el hilo principal esto evitara el problema de
     * {@link android.os.NetworkOnMainThreadException}.
     *
     * @param taskCallback {@link TaskCallback}
     * @return {@link EasyHTTP} retorna la instancia que realizo esta solicitud. Si se llama este
     * metodo luego de {@link Builder#build()} o {@link PostBuilder#build()} permite almacenar
     * la instacia para volver a realizar la llamada usando los mismo parámetros.
     */
    public EasyHTTP executeAsync(TaskCallback taskCallback) {
        Executor.executeAsync(this, taskCallback);
        return this;
    }

    /**
     * Ejecuta una llamada de la solicitud de manera sincrónica.
     * <p>
     * Esto retorna una instancia de {@link Response} con el resultado obtenido pero congela
     * el hilo sobre el cual sea ejecutado este método.
     *
     * @return Instancia de {@link Response} con el resultado obtenido.
     */
    public Response execute() throws NoSuchAlgorithmException, IOException, KeyManagementException {
        return Executor.executeCall(this);
    }

    /**
     * Método auto generado para retornar los valores de la clase en formato de cadena de texto.
     *
     * @return {@link String} con los valores de la clase.
     */
    @Override
    public String toString() {
        return "EasyHTTP{" +
                "connectTimeout=" + connectTimeout +
                ", readTimeout=" + readTimeout +
                ", url='" + url + '\'' +
                ", forceSSL=" + forceSSL +
                ", requestType=" + requestType +
                ", headerMap=" + headerMap +
                ", filesMap=" + filesMap +
                ", parametersMap=" + parametersMap +
                ", rawBody='" + rawBody + '\'' +
                '}';
    }

    /**
     * Builder para crear una solicitud GET.
     */
    public static class Builder {
        int connectTimeout = 15;
        int readTimeout = 15;
        String url;
        boolean forceSSL = false;
        RequestType requestType = RequestType.GET;
        HashMap<String, String> headerMap = new HashMap<>();
        ;

        /**
         * Constructor public de la clase interna Builder.
         *
         * @param url {@link String} URL hacia la cual se enviara la solicitud.
         */
        public Builder(String url) {
            this.url = url;
        }

        /**
         * Constructor privado de la clase interna Builder.
         * Se usa al crear una instancia de {@link PostBuilder}.
         *
         * @param builder {@link Builder} builder para iniciar los atributos de la clase.
         */
        private Builder(Builder builder) {
            this.connectTimeout = builder.connectTimeout;
            this.readTimeout = builder.readTimeout;
            this.url = builder.url;
            this.forceSSL = builder.forceSSL;
            this.requestType = builder.requestType;
            this.headerMap = builder.headerMap;
        }

        /**
         * Establece el valor de espera de conexión.
         *
         * @param connectTimeout int Tiempo en segundos que se esperará antes de lanzar {@link java.net.SocketTimeoutException}
         * @return {@link Builder} con el nuevo valor del atributo establecido.
         */
        public Builder setConnectionTimeout(int connectTimeout) {
            this.connectTimeout = connectTimeout;
            return this;
        }

        /**
         * Establece el valor de espera de lectura.
         *
         * @param readTimeout int Tiempo en segundos que se esperará antes de lanzar {@link java.net.SocketTimeoutException}
         * @return {@link Builder} con el nuevo valor del atributo establecido.
         */
        public Builder setReadTimeout(int readTimeout) {
            this.readTimeout = readTimeout;
            return this;
        }

        /**
         * Forzara a la ejecución a ser de tipo SSL y además cambiara la URL de "http://" a "https://".
         *
         * @return {@link Builder} con el nuevo valor del atributo establecido a true.
         */
        public Builder forceSSL() {
            this.forceSSL = true;
            return this;
        }

        /**
         * Añade un valor a la Cabecera de la solicitud.
         *
         * @param name   {@link String} llave para la cabecera.
         * @param header {@link String} valor asociado a la llave.
         * @return {@link Builder} con el nuevo valor del atributo establecido.
         */
        public Builder addHeader(String name, String header) {
            this.headerMap.put(name, header);
            return this;
        }

        /**
         * Transforma el {@link Builder} en una instancia de su Hijo {@link PostBuilder}
         * para crear una solicitud POST.
         *
         * @return {@link PostBuilder} con los valores que el {@link Builder} tuviese hasta
         * la llamada de este método.
         */
        public PostBuilder post() {
            this.requestType = RequestType.POST;
            return new PostBuilder(this);
        }

        /**
         * Construye una instancia de {@link EasyHTTP} con los parámetros añadidos
         * al Builder.
         *
         * @return Instancia de {@link EasyHTTP}.
         */
        public EasyHTTP build() {
            return new EasyHTTP(this);
        }

        public Response execute() throws NoSuchAlgorithmException, IOException, KeyManagementException {
            return build().execute();
        }

        public EasyHTTP executeAsync(TaskCallback taskCallback) {
            EasyHTTP easyHTTP = this.build();
            easyHTTP.executeAsync(taskCallback);
            return easyHTTP;
        }
    }

    /**
     * Builder para crear una solicitud POST.
     * <p>
     * La principal diferencia es que una solicitud post lleva parámetros y contenido en
     * un RequestBody en la solicitud, por ende este Builder permite añadir parámetros y archivos.
     */
    public static class PostBuilder extends Builder {
        HashMap<String, File> filesMap = new HashMap<>();
        HashMap<String, Object> parametersMap = new HashMap<>();
        String rawBody;

        /**
         * Constructor privado de {@link PostBuilder}.
         * Por defecto esta solicitud contiene un {@link RequestType#POST}.
         *
         * @param builder {@link Builder} con los valores mínimos para crear este Builder.
         */
        private PostBuilder(Builder builder) {
            super(builder);
        }

        /**
         * Añade un parámetro en el formato {llave-valor} al cuerpo de la solicitud.
         *
         * @param name  {@link String} llave para el parámetro
         * @param value {@link String} datos asociados a la llave.
         * @return {@link PostBuilder} con el parámetro añadido.
         */
        public PostBuilder addParameter(String name, String value) {
            this.parametersMap.put(name, value);
            return this;
        }

        /**
         * Añade un parámetro en el formato {llave-valor} al cuerpo de la solicitud.
         *
         * @param name  {@link String} llave para el parámetro
         * @param value {@link Integer} datos asociados a la llave.
         * @return {@link PostBuilder} con el parámetro añadido.
         */
        public PostBuilder addParameter(String name, Integer value) {
            this.parametersMap.put(name, value);
            return this;
        }

        /**
         * Añade un parámetro en el formato {llave-valor} al cuerpo de la solicitud.
         *
         * @param name  {@link String} llave para el parámetro
         * @param value {@link Float} datos asociados a la llave.
         * @return {@link PostBuilder} con el parámetro añadido.
         */
        public PostBuilder addParameter(String name, Float value) {
            this.parametersMap.put(name, value);
            return this;
        }

        /**
         * Añade un parámetro en el formato {llave-valor} al cuerpo de la solicitud.
         *
         * @param name  {@link String} llave para el parámetro
         * @param value {@link Long} datos asociados a la llave.
         * @return {@link PostBuilder} con el parámetro añadido.
         */
        public PostBuilder addParameter(String name, Long value) {
            this.parametersMap.put(name, value);
            return this;
        }

        /**
         * Añade un parámetro en el formato {llave-valor} al
         * +cuerpo de la solicitud.
         *
         * @param name  {@link String} llave para el parámetro
         * @param value {@link Boolean} datos asociados a la llave.
         * @return {@link PostBuilder} con el parámetro añadido.
         */
        public PostBuilder addParameter(String name, Boolean value) {
            this.parametersMap.put(name, value);
            return this;
        }


        /**
         * Añade un archivo asociado con la llave ingresada a la solicitud.
         * Este método es similar a {@link PostBuilder#addParameter(String, String)} con la
         * diferencia de que un archivo es añadido en vez de una cadena de texto.
         *
         * @param name {@link String} llave para el cuerpo de la solicitud.
         * @param file {@link File} Archivo asociado a la llave.
         * @return {@link PostBuilder} con el archivo añadido.
         */
        public PostBuilder addFile(String name, File file) {
            this.filesMap.put(name, file);
            return this;
        }

        /**
         * Añade una cadena de texto al cuerpo de la solicitud.
         * Advertencia: Establecer este valor provocara que los parámetros y archivos sean ignorados
         * al crear la solicitud.
         *
         * @param rawBody {@link String } para el cuerpo de la solicitud.
         * @return {@link PostBuilder} con el valor establecido.
         */
        public PostBuilder setRawBody(String rawBody) {
            this.rawBody = rawBody;
            return this;
        }

        public EasyHTTP build() {
            return new EasyHTTP(this);
        }

        public Response execute() throws NoSuchAlgorithmException, IOException, KeyManagementException {
            return build().execute();
        }

        public EasyHTTP executeAsync(TaskCallback taskCallback) {
            EasyHTTP easyHTTP = this.build();
            easyHTTP.executeAsync(taskCallback);
            return easyHTTP;
        }
    }
}
