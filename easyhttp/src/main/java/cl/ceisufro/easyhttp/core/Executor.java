package cl.ceisufro.easyhttp.core;

import android.os.AsyncTask;
import android.text.TextUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import cl.ceisufro.easyhttp.model.MimeType;
import cl.ceisufro.easyhttp.model.RequestType;
import cl.ceisufro.easyhttp.model.Response;

/**
 * Created by Luis Andrés Jara Castillo on 12-09-17.
 * <p>
 * Clase encargada de resolver y ejecutar solicitudes de tipo
 * {@link EasyHTTP}.
 * Permite realizar la ejecución de manera sincrónica para casos
 * en los que se hace la llamada desde un hilo que no es el principal.
 * También se puede realizar la ejecución de forma asíncrona para
 * evitar bloquear algún hilo, por ejemplo, el hilo principal de la aplicación
 * (Revisar {@link android.os.NetworkOnMainThreadException}).
 */

class Executor {
    private static String boundary;
    private static final String lineEnd = "\r\n";
    private static final String charSet = "UTF-8";

    /**
     * Método que realiza una llamada sincrónica usando la información
     * de la instancia de {@link EasyHTTP} entregada como parámetro.
     *
     * @param easyHTTP {@link EasyHTTP} con la información de la solicitud HTTP.
     * @return {@link Response} respuesta obtenida de la ejecución,
     */
    static Response executeCall(EasyHTTP easyHTTP) throws IOException,
            NoSuchAlgorithmException, KeyManagementException {
        Response output = null;
        boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
        String urlString = easyHTTP.getUrl();
        if ((!easyHTTP.isSSLForced() && !TextUtils.isEmpty(urlString) && !urlString.contains("https://"))) {
            //Validar URL
            if (!TextUtils.isEmpty(urlString)) {
                if (!urlString.contains("http://")) {
                    urlString = "http://" + urlString;
                }
            } else {
                return null;
            }
            URL url = new URL(urlString);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            //Connection Config
            httpURLConnection.setConnectTimeout(easyHTTP.getConnectTimeout() * 1000);
            httpURLConnection.setReadTimeout(easyHTTP.getReadTimeout() * 1000);
            Set<String> keys = easyHTTP.getHeaderMap().keySet();
            for (String key : keys) {
                httpURLConnection.setRequestProperty(key, easyHTTP.getHeaderMap().get(key));
            }
            if (easyHTTP.getRequestType() == RequestType.GET) {
                httpURLConnection.setRequestMethod("GET");
            } else {
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                if (TextUtils.isEmpty(easyHTTP.getRawBody())) {
                    httpURLConnection.setRequestProperty("Content-Type",
                            "multipart/form-data; boundary=" + boundary);
                } else {
                    httpURLConnection.setRequestProperty("Content-Type",
                            "application/x-www-form-urlencoded");
                }
                httpURLConnection.setRequestProperty("User-Agent", "Android EasyHTTP Request Execution");
                writeRequestBodyOutputStream(httpURLConnection.getOutputStream(), easyHTTP);
            }
            //Build Response
            int code = httpURLConnection.getResponseCode();
            InputStream inputStream = httpURLConnection.getInputStream();
            long contentLenght = httpURLConnection.getContentLength();
            MimeType mimeType = new MimeType(httpURLConnection.getContentType());
            Charset charset = null;
            if (!TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
                charset = Charset.forName(httpURLConnection.getContentEncoding());
            } else {
                if (!httpURLConnection.getContentType().isEmpty() && httpURLConnection.getContentType().contains("charset=")) {
                    String split[] = httpURLConnection.getContentType().split("charset=");
                    if (split.length > 0) {
                        charset = Charset.forName(split[1]);
                    }
                }
            }
            output = new Response(code, inputStream, contentLenght, mimeType, charset);
        } else if (easyHTTP.isSSLForced() || (!easyHTTP.isSSLForced() && !TextUtils.isEmpty(urlString) && urlString.contains("https://"))) {
            //Validar URL
            if (!TextUtils.isEmpty(urlString)) {
                if (!urlString.contains("https://") && !urlString.contains("http://")) {
                    urlString = "https://" + urlString;
                }
            } else {
                return null;
            }
            URL url = new URL(urlString);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            //Connection Config
            httpsURLConnection.setConnectTimeout(easyHTTP.getConnectTimeout() * 1000);
            httpsURLConnection.setReadTimeout(easyHTTP.getReadTimeout() * 1000);
            //SSL CONFIG
            SSLContext sc;
            sc = SSLContext.getInstance("TLS");
            sc.init(null, null, new java.security.SecureRandom());
            httpsURLConnection.setSSLSocketFactory(sc.getSocketFactory());
            Set<String> keys = easyHTTP.getHeaderMap().keySet();
            for (String key : keys) {
                httpsURLConnection.setRequestProperty(key, easyHTTP.getHeaderMap().get(key));
            }
            if (easyHTTP.getRequestType() == RequestType.GET) {
                httpsURLConnection.setRequestMethod("GET");
            } else {
                httpsURLConnection.setUseCaches(false);
                httpsURLConnection.setDoOutput(true);
                httpsURLConnection.setDoInput(true);
                if (TextUtils.isEmpty(easyHTTP.getRawBody())) {
                    httpsURLConnection.setRequestProperty("Content-Type",
                            "multipart/form-data; boundary=" + boundary);
                } else {
                    httpsURLConnection.setRequestProperty("Content-Type",
                            "application/x-www-form-urlencoded");
                }
                httpsURLConnection.setRequestProperty("User-Agent", "Android EasyHTTP Request Execution");
                writeRequestBodyOutputStream(httpsURLConnection.getOutputStream(), easyHTTP);
            }
            //Build Response
            int code = httpsURLConnection.getResponseCode();
            InputStream inputStream = httpsURLConnection.getInputStream();
            long contentLenght = 0;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                contentLenght = httpsURLConnection.getContentLengthLong();
            } else {
                contentLenght = httpsURLConnection.getContentLength();
            }
            MimeType mimeType = new MimeType(httpsURLConnection.getContentType());
            Charset charset = null;
            if (!TextUtils.isEmpty(httpsURLConnection.getContentEncoding())) {
                charset = Charset.forName(httpsURLConnection.getContentEncoding());
            } else {
                if (!httpsURLConnection.getContentType().isEmpty() && httpsURLConnection.getContentType().contains("charset=")) {
                    String split[] = httpsURLConnection.getContentType().split("charset=");
                    if (split.length > 0) {
                        charset = Charset.forName(split[1]);
                    }
                }
            }
            output = new Response(code, inputStream, contentLenght, mimeType, charset);
        }
        return output;
    }

    /**
     * Método privado para escribir los parámetros y archivos dentro del {@link OutputStream} de la instancia
     * de {@link HttpURLConnection}/{@link HttpsURLConnection}.
     *
     * @param outputStream {@link OutputStream} de la instancia de {@link HttpURLConnection}/{@link HttpsURLConnection}.
     * @param easyHTTP     {@link EasyHTTP} con los parámetros para añadir al {@link OutputStream}.
     */
    private static void writeRequestBodyOutputStream(OutputStream outputStream, EasyHTTP easyHTTP) {
        try {
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream, "UTF-8"),
                    true);
            if (TextUtils.isEmpty(easyHTTP.getRawBody())) {
                //Writing files into the Connection OutputStream
                Set<String> keys = easyHTTP.getFilesMap().keySet();
                for (String key : keys) {
                    File file = easyHTTP.getFilesMap().get(key);
                    if (file != null && file.isFile()) {
                        String fileName = file.getName();
                        writer.append("--").append(boundary).append(lineEnd);
                        writer.append("Content-Disposition: form-data; name=\"").append(key).append("\"; filename=\"").append(fileName).append("\"")
                                .append(lineEnd);
                        writer.append("Content-Type: ").append(URLConnection.guessContentTypeFromName(fileName))
                                .append(lineEnd);
                        writer.append("Content-Transfer-Encoding: binary").append(lineEnd);
                        writer.append(lineEnd);
                        writer.flush();
                        FileInputStream inputStream = new FileInputStream(file);
                        byte[] buffer = new byte[4096];
                        int bytesRead = -1;
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }
                        outputStream.flush();
                        inputStream.close();
                        writer.append(lineEnd);
                    }
                }
                //Writing params into the Connection OutputStream
                for (String key : easyHTTP.getParametersMap().keySet()) {
                    Object value = easyHTTP.getParametersMap().get(key);
                    writer.append("--").append(boundary).append(lineEnd);
                    writer.append("Content-Disposition: form-data; name=\"").append(key).append("\"")
                            .append(lineEnd);
                    writer.append("Content-Type: text/plain; charset=" + charSet).append(
                            lineEnd);
                    writer.append(lineEnd);
                    String string = value.toString();
                    writer.append(string).append(lineEnd);
                }
                writer.append("--").append(boundary).append("--").append(lineEnd);
            } else {
                String value = easyHTTP.getRawBody();
                writer.append(value).append("\n");
            }
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que realiza una llamada asíncrona usando la información
     * de la instancia de {@link EasyHTTP} entregada como parámetro.
     * Es necesario entregar una implementación de {@link TaskCallback}
     * como parámetro, debido a que la respuesta a la solicitud es entregada
     * mediante esta interface.
     *
     * @param easyHTTP     {@link EasyHTTP} con la información de la solicitud HTTP.
     * @param taskCallback {@link TaskCallback} para recibir los cambios en la ejecución de la solicitud.
     */
    static void executeAsync(EasyHTTP easyHTTP, TaskCallback taskCallback) {
        new AsyncCaller(easyHTTP, taskCallback).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    /**
     * Clase interna para la ejecución asíncrona de la solicitud.
     */
    private static class AsyncCaller extends AsyncTask<Void, Void, Response> {
        private TaskCallback taskCallback;
        private EasyHTTP easyHTTP;

        /**
         * Constructor de la clase.
         *
         * @param easyHTTP     {@link EasyHTTP} para realizar la llamada de manera asíncrona.
         * @param taskCallback {@link TaskCallback} para devolver los valores.
         */
        AsyncCaller(EasyHTTP easyHTTP, TaskCallback taskCallback) {
            this.taskCallback = taskCallback;
            this.easyHTTP = easyHTTP;
        }

        /**
         * Método sobrescrito de AsyncTask que marca el inicio de la tarea.
         * Usando {@link TaskCallback} se realiza una llamada a el
         * método {@link TaskCallback#onStart()}.
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.taskCallback.onStart();
        }

        /**
         * Este método realiza la ejecución de la tarea a través de un hilo secundario.
         *
         * @return {@link Response}
         */
        @Override
        protected Response doInBackground(Void... voids) {
            try {
                return executeRequest();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Método sobrescrito de AsyncTask que marca el final de la tarea.
         * Usando {@link TaskCallback} se realiza una llamada a el
         * método {@link TaskCallback#onFinish()} ()}.
         */
        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (response != null) {
                taskCallback.onResult(response);
            }
            taskCallback.onFinish();
        }

        /**
         * Método privado que efectúa la llamada a {@link Executor#executeCall(EasyHTTP)}.
         * Este método es solo con la finalidad de capturar algun error y enviar el callback
         * correspondiente {@link TaskCallback#onError(Throwable)}
         *
         * @return {@link Response} con los datos obtenidos de la llamada o null si ocurre algún error.
         */
        private Response executeRequest() {
            Response output = null;
            try {
                output = executeCall(easyHTTP);
            } catch (Exception e) {
                taskCallback.onError(e);
            }
            return output;
        }
    }
}
