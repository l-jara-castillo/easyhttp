package cl.ceisufro.easyhttp.model;

/**
 * Created by Luis Andrés Jara Castillo on 12-09-17.
 *
 * Este enum tiene como finalidad la de determinar el tipo
 * de Consulta que se realizara.
 * Para la primera versión se añadieron GET y POST solamente
 * y se espera que se añadan el resto en futuras versiones.
 * GET y POST son las mas convencionales y además se puede
 * realizar todo tipo de consultas con estas.
 */

public enum RequestType {
    GET,
    POST
}
