package cl.ceisufro.easyhttp.model;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Objects;

/**
 * Creado por Luis Andrés Jara Castillo on 11-09-17.
 * <p>
 * Clase para representar una respuesta de una solicitud.
 * Contiene algunos valores útiles además del contenido
 * obtenido de la consulta realizada.
 */

public class Response {
    private int code;
    private byte[] bodyByteArray;
    private long contentLenght;
    private MimeType contentMimeType;
    private Charset charset;

    /**
     * Constructor por defecto de la clase.
     *
     * @param code            int - Código HTTP obtenido como resultado.
     * @param inputStream     {@link InputStream} - InputStream para extraer el contenido de la respuesta.
     * @param contentLenght   long - Largo en bytes del contenido obtenido.
     * @param contentMimeType {@link MimeType} - Tipo de dato del contenido de la respuesta. Por ejemplo:
     *                        "image/jpeg".
     * @param charset   {@link Charset}  - Largo en bytes del contenido obtenido.
     */
    public Response(int code, InputStream inputStream, long contentLenght, MimeType contentMimeType, Charset charset) {
        this.code = code;
        this.contentLenght = contentLenght;
        this.contentMimeType = contentMimeType;
        this.charset = charset;
        this.bodyByteArray = getContentAsByteArray(inputStream);
    }

    /**
     * Metodo privado para extraer el contenido en forma de {@link InputStream} y guardarlo en un
     * arreglo de byte.
     *
     * @param inputStream {@link InputStream} - Contenido obtenido de la consulta HTTP.
     * @return byte[] - Contenido en un arreglo de byte[] o null si no es posible extraer el contenido.
     */
    private byte[] getContentAsByteArray(InputStream inputStream) {
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[2048];
            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            return buffer.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retorna el código HTTP de la respuesta.
     * <p>
     * Vease: <a href="https://es.wikipedia.org/wiki/Anexo:C%C3%B3digos_de_estado_HTTP">Códigos HTTP</a>
     *
     * @return int - Código de estado HTTP de la consulta realizada.
     */
    public int getCode() {
        return code;
    }

    /**
     * Retorna el largo en bytes que tiene el contenido obtenido de la respuesta.
     *
     * @return long - Largo del contenido en bytes.
     */
    public long getContentLenght() {
        return contentLenght;
    }

    /**
     * Retorna una instancia de {@link MimeType} que representa el tipo de contenido obtenido.
     * Por Ejemplo: "image/jpeg" el cual es dividido en Tipo("image") y Subtipo("jpeg").
     *
     * @return {@link MimeType} - Instancia de MediaType con el tipo de contenido.
     */
    public MimeType getContentMimeType() {
        return contentMimeType;
    }

    /**
     * Retorna el contenido obtenido en forma de cadena de caracteres.
     * Por ejemplo: Si el contenido es el código HTML de un sitio, al utilizar este método
     * se podrá obtener el código dentro de una instancia de {@link String}.
     *
     * @return {@link String} - Contenido obtenido en una cadena de caracteres.
     */
    public String getStringBody() {
        return new String(bodyByteArray);
    }

    /**
     * Retorna el contenido obtenido en forma de un arreglo de bytes.
     * Esto es util cuando el contenido obtenido es una imagen o video, un
     * arreglo de byte permitirá guardar el contenido en un Archivo o pasarlo a
     * un {@link android.graphics.Bitmap}.
     *
     * @return byte[] - Contenido obtenido en un arreglo de byte.
     */
    public byte[] getBodyByteArray() {
        return bodyByteArray;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Response)) return false;
        Response response = (Response) o;
        return getCode() == response.getCode() &&
                getContentLenght() == response.getContentLenght() &&
                Arrays.equals(getBodyByteArray(), response.getBodyByteArray()) &&
                Objects.equals(getContentMimeType(), response.getContentMimeType()) &&
                Objects.equals(charset, response.charset);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode(), getBodyByteArray(), getContentLenght(), getContentMimeType(), charset);
    }

    @Override
    public String toString() {
        return "Response{" +
                "code=" + code +
                ", contentLenght=" + contentLenght +
                ", contentMimeType=" + contentMimeType +
                ", charset=" + charset +
                '}';
    }
}
