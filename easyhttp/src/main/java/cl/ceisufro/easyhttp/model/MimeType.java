package cl.ceisufro.easyhttp.model;

import java.util.Objects;

/**
 * Created by Luis Andrés Jara Castillo on 11-09-17.
 * <p>
 * Esta clase cumple la finalidad de facilitar el conocimiento del
 * tipo de contenido obtenido de una consulta http y además
 * facilitar la extracción del tipo de contenido retornado por
 * {@link javax.net.ssl.HttpsURLConnection#getContentType()}
 * y {@link java.net.HttpURLConnection#getContentType()}.
 */

public class MimeType {
    private String type;
    private String subtype;

    /**
     * Constructor de la clase.
     *
     * @param type    {@link String} - Tipo de archivo. Ej: "image", "audio", etc.
     * @param subtype {@link String} - Subtipo de archivo. Ej: "mpeg", "jpeg", etc.
     */
    public MimeType(String type, String subtype) {
        this.type = type;
        this.subtype = subtype;
    }

    /**
     * Constructor de la clase. Extrae el tipo y subtipo de un string con, o que contenga,
     * el siguiente formato: "tipo/subtipo" ej: "image/jpeg".
     *
     * @param mediaType {@link String} - Cadena de texto con el formato especificado.
     */
    public MimeType(String mediaType) {
        if (mediaType != null) {
            String split[] = mediaType.split("/");
            this.type = split.length > 0 ? split[0].trim() : "";
            this.subtype = split.length > 0 ? split[1].trim() : "";
            if (this.subtype.contains(";")) {
                split = this.subtype.split(";");
                this.subtype = split.length > 0 ? split[0] : "";
            } else if (this.subtype.contains("charset=")) {
                split = this.subtype.split("charset=");
                this.subtype = split.length > 0 ? split[0] : "";
            }
        }
    }

    /**
     * Retorna el tipo de contenido que se obtuvo de la respuesta.
     *
     * @return {@link String} - Tipo del contenido
     */
    public String getType() {
        return type;
    }

    /**
     * Retorna el tipo de contenido que se obtuvo de la respuesta.
     *
     * @return {@link String} - Subtipo del contenido
     */
    public String getSubtype() {
        return subtype;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MimeType)) return false;
        MimeType mimeType = (MimeType) o;
        return Objects.equals(getType(), mimeType.getType()) &&
                Objects.equals(getSubtype(), mimeType.getSubtype());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getSubtype());
    }

    @Override
    public String toString() {
        return "MediaType{" +
                ", type='" + type + '\'' +
                ", subtype='" + subtype + '\'' +
                '}';
    }
}
